#!/bin/bash
#################################
###### Attic backup script ######
#################################
# Author: Sascha Winkelhofer
# Daily cronjob at 1.59 pm
# 59 1 * * * /bin/bash /root/attic.sh 2>&1 > /dev/null
# Sends mail to zawiw.webadmin@uni-ulm.de from erwin.hutterer@uni-ulm.de
# via mail.uni-ulm.de to avoid installation of potential harmful mail-packages

# Set paths
smb_uri="storage01.zawiw.uni-ulm.de/Freigaben"
username="backupuser"
password="3u5XjLTE"
# backup_root="/root/backup/backup/backup.attic"
backup_root="/root/backup/ZAWiW/Temp/BackupUser"
to_backup="/export/data"

# Generate mail-header
echo "helo attic.zawiw.de" > log.txt
echo "mail from:<erwin.hutterer@uni-ulm.de>" >> log.txt
echo "rcpt to:<zawiw.webadmin@uni-ulm.de>" >> log.txt
echo "date:" $(date -R) >> log.txt
echo "DATA" >> log.txt
echo "Subject: Attic Backup" >> log.txt
echo "##################" >> log.txt
echo "## ATTIC BACKUP ##" >> log.txt
echo "##################" >> log.txt

# Mount NAS for backup
# sshfs -o follow_symlinks -o reconnect -o no_check_root -o allow_root -o noauto_cache,sync_read backup@134.60.40.29:/home /root/backup/ >> log.txt 2>&1

# SSHFS mount no longer available
# CIFS mount instead
mount //$smb_uri -o user=$username,password=$password /root/backup

# Create new backup
echo "Creating current Backup" >> log.txt
before=$(date +%s)
/usr/bin/attic create $backup_root::$(date +%Y-%m-%d) $to_backup >> log.txt 2>&1
after=$(date +%s)
echo "    elapsed time:" $(($after - $before)) "seconds" >> log.txt

# Purge old backups, keep 7 daily, 4 weekly & 6 monthly backups
echo "Deleting old Backups" >> log.txt
before=$(date +%s)
/usr/bin/attic prune $backup_root --keep-daily=7 --keep-weekly=4 --keep-monthly=6 >> log.txt 2>&1
after=$(date +%s)
echo "    elapsed time:" $(($after - $before)) "seconds" >> log.txt

# Unmount NAS
umount /root/backup >> log.txt 2>&1

echo "." >> log.txt
echo "quit" >> log.txt

# Send Mail via Uni-Mailserver
cat log.txt | nc mail.uni-ulm.de 25

# Delete Logfile
rm log.txt
