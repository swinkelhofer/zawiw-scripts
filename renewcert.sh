mv /etc/nginx/sites-enabled/apache /etc/nginx/sites-enabled/.apache
service nginx reload
#while read domain
for domain in `cat /var/www/html/activeDomains`
do
	echo "renewing cert for $domain"
	/usr/bin/docker run -i --rm -p 4431:443 -p 801:80 --name letsencrypt -v /etc/letsencrypt:/etc/letsencrypt -v /var/lib/letsencrypt:/var/lib/letsencrypt -v /export/.well-known:/var/www/html/.well-known zawiw/letsencrypt --server https://acme-v01.api.letsencrypt.org/directory -a webroot --webroot-path /var/www/html --email zawiw.webadmin@uni-ulm.de --text --agree-tos -d $domain auth --renew-by-default
done
#< /var/www/html/activeDomains



mv /etc/nginx/sites-enabled/.apache /etc/nginx/sites-enabled/apache
service nginx reload
